<?php
/**
 * Header template
 *
 * @since   1.0.0
 * @package _rock
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_rock' ); ?></a>

	<header role="banner">
		<?php _rock_template_part( 'header/content' ); ?>
		<?php _rock_template_part( 'header/navigation' ); ?>
	</header>

	<div id="content">
