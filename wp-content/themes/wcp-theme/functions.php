<?php
/**
 * _rock functions and definitions.
 *
 * @since   1.0.0
 * @package _rock
 */

/**
 * Define stuff
 */
define( '_ROCK_THEME_VERSION' , '1.0.0'                                   );
define( '_ROCK_DIR'           , get_template_directory()                  );
define( '_ROCK_DIR_URI'       , get_template_directory_uri()              );
define( '_ROCK_GAPI'          , 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' ); // also update in custom plugin if needed!

/**
 * Get includes - theme-only functionality
 */
$includes = [
	'functions',
	'setup',
	'class-scripts',
	'class-404',
	'class-acf',
	'class-blog',
	'class-button',
	'class-customizer',
	'class-contacts',
	'class-socials',
	'class-gravity-forms',
	'class-layouts',
	'image-sizes',
	'component/cover-image',
	'component/template-tags',
];
foreach ( $includes as $include ) {
	require _ROCK_DIR . '/includes/' . $include . '.php';
}

// start up stuff
_Rock_Layouts::instance();

/**
 * Set user agent so we can get_file_contents and stuff
 */
ini_set( 'user_agent', "_rockAgent" );


/**
 * Add contact info customizer controls
 *
 * @todo remove Miami -- this is just an example (unless it's needed)
 */
_Rock_Contacts::add_group( __( 'Orlando' , '_rock' )        );
_Rock_Contacts::add_group( __( 'Miami'   , '_rock' ), 'mia' );


/**
 * Add socials (also registers customizer stuff)
 */
_Rock_Socials::add_profiles([
	__( 'Facebook'    , '_rock' ) => 'facebook',
	__( 'Twitter'     , '_rock' ) => 'twitter',
	__( 'YouTube'     , '_rock' ) => 'youtube',
	__( 'Instagram'   , '_rock' ) => 'instagram',
	__( 'Google Plus' , '_rock' ) => 'google-plus',
]);
