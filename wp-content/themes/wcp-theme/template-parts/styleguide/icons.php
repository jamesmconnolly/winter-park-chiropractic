<?php
/**
 * Styleguide's icon section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @since   1.0.0
 * @package _rock
 */

$icons = [
	'chevron-right',
	'calendar-alt',
	'search',
	'user',
	'comments',
	'plus',
	'bars',
	'file-alt',
	'check',
];
?>

<h3><?php _ex( 'Icons', 'styleguide' ); ?></h3>

<p><?php printf( _x( '%sFont Awesome 5 Pro%s: Regular', 'styleguide' ), '<b>', '</b>' ); ?>

<ul class="icons">
	<?php foreach ( $icons as $icon ) : ?>
		<li><i class="fas fa-<?php echo $icon; ?>"></i><?php echo $icon; ?></li>
	<?php endforeach; ?>
</ul>
