<?php
/**
 * Styleguide's colors section
 *
 * $args stuff is passed in from main styleguide template
 *
 * @see     https://kb.yoast.com/kb/implement-wordpress-seo-breadcrumbs/ for breadcrumbs
 * @since   1.0.0
 * @package _rock
 */
?>

<h3><?php _ex( 'Pagination (Simulated)', 'styleguide' ); ?></h3>

<dl>

	<dt><?php _ex( 'Prev / Next', 'styleguide' ); ?></dt>
	<dd>
		<nav class="navigation posts-navigation" role="navigation">
			<h2 class="screen-reader-text">Posts navigation</h2>
			<div class="nav-links">
				<div class="nav-previous"><a href="#"><span class="icon"><i class="fas fa-chevron-left"></i></span><span class="text">Previous</span></a></div>
				<div class="nav-next"><a href="#"><span class="text">Next</span><span class="icon"><i class="fas fa-chevron-right"></i></span></a></div>
			</div>
		</nav>
	</dd>


	<dt><?php _ex( 'Numeric', 'styleguide' ); ?></dt>
	<dd>
		<nav class="posts-navigation numbered-pagination"><ul class="page-numbers">
			<li><a class="prev page-numbers" href="#"><i class="fas fa-chevron-left"></i>Previous</a></li>
			<li><a class="page-numbers" href="#"><span class="screen-reader-text">Page </span>1</a></li>
			<li><span aria-current="page" class="page-numbers current"><span class="screen-reader-text">Page </span>2</span></li>
			<li><a class="page-numbers" href="#"><span class="screen-reader-text">Page </span>3</a></li>
			<li><a class="page-numbers" href="#"><span class="screen-reader-text">Page </span>4</a></li>
			<li><span class="page-numbers dots">…</span></li>
			<li><a class="page-numbers" href="#"><span class="screen-reader-text">Page </span>10</a></li>
			<li><a class="page-numbers" href="#"><span class="screen-reader-text">Page </span>11</a></li>
			<li><a class="next page-numbers" href="#"><i class="fas fa-chevron-right"></i>Next</a></li>
		</ul></nav>
	</dd>


	<dt><?php _ex( 'Breadcrumb (Yoast)', 'styleguide' ); ?></dt>
	<dd>
		<nav class="breadcrumb"><span><span>
			<a href="#">Home</a> <i class="fas fa-chevron-right"></i>
			<span>
				<a href="#">Breadcrumb Parent</a> <i class="fas fa-chevron-right"></i>
				<span class="breadcrumb_last">Styleguide</span>
			</span>
		</span></span></nav>
	</dd>

</dl>
