<?php
/**
 * Header "top" with logo and mobile nav toggle
 *
 * @since   1.0.0
 * @package _rock
 */
?>

<div class="header-content">
	<!-- site logo -->
	<a class="site-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

		<?php _rock_svg( 'logo_winter-park-chiropractic' ); ?>
		<p><?php bloginfo( 'name' ); ?></p>
		<p><?php bloginfo( 'description', 'display' ); ?></p>
	</a>

	<!-- phone number -->
	<div class="phone-cta">
		<a href="#"><span class="cta-text">Call today 406-339-2225</span> <i class="fas fa-phone"></i></a>
	</div>

	<!-- mobile navigation toggle -->
	<button id="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="hamburger"></span><span class="toggle-text"></span><span class="screen-reader-text"><?php esc_html_e( 'Menu', '_rock' ); ?></span></button>
</div>