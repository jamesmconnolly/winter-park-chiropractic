<?php
/**
 * Header navigation
 *
 * @since   1.0.0
 * @package _rock
 */
?>

<nav>
	<?php wp_nav_menu([
		'theme_location' => 'primary',
		'menu_id'        => 'primary-menu',
		'depth'          => 3,
		'fallback_cb'    => false,
	]); ?>
</nav>
