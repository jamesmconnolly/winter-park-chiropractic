<?php
/**
 * Sample Home page "services" template part
 *
 * @since   1.0.0
 * @package _rock
 */
extract( $meta );

$attrs = _rock_attrs( array(
	'id'    => $custom_id,
	'class' => array(
		'CLASS-NAME-FOR-SECTION-THOUGH',
		$custom_classes,
	),
));
?>

<section <?php echo $attrs; ?>>
	<div class="wrap">

		<!-- title -->
		<?php echo $title ? "<h2>$title</h2>" : ''; ?>

		<!-- body -->
		<?php _rock_entry_content( $body ); ?>

		<!-- button -->
		<?php _rock_button( $button ); ?>

	</div>
</section>
