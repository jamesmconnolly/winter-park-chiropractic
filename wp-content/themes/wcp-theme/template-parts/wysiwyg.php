<?php
/**
 * WYSIWYG template part
 *
 * @since   1.0.0
 * @package _rock
 */
extract( $meta );

$attrs = _rock_attrs( array(
	'id'    => $custom_id,
	'class' => array(
		'_rock-wysiwyg',
		$custom_classes,
	),
));
?>

<section <?php echo $attrs; ?>>
	<div class="wrap">

		<!-- body -->
		<?php _rock_entry_content( $content ); ?>

	</div>
</section>
