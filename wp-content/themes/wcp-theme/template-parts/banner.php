<?php
/**
 * Page Banner
 *
 * @since   1.0.0
 * @package _rock
 */
extract( $meta );

/**
 * CLEAR IF NOT NEEDED! JUST AN EXAMPLE.
 * This will add the "banner" class and CONDITIONALLY add the has-bg-image class
 * if the banner's background image field is populated.
 */
$attrs = _rock_attrs([
	'class'	=> [
		'banner',
		! empty( $bg_image ) ? 'has-bg-image' : null,
	],
]);
?>

<header <?php echo $attrs; ?>>
	<div class="wrap">

		<?php
		// meta field
		if ( ! empty( $title ) ) :
			echo "<h1>$title</h1>";

		// search
		elseif ( is_search() ) :
		?>
			<h1 class="search-title"><?php printf( esc_html__( 'Search: %s', '_rock' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php

		// archive or posts page without banner fields
		elseif ( is_archive() || is_home() ) :
			the_archive_title( '<h1 class="archive-title">', '</h1>' );

		// default
		else :
			the_title( '<h1>', '</h1>' );
		endif;


		// subtitle meta field
		if ( ! empty( $subtitle ) ) :
			echo "<h2>$subtitle</h2>";

		// archive/posts page without banner fields
		elseif ( is_archive() ) :
			the_archive_description( '<h2 class="archive-subtitle">', '</h2>' );
		endif;
		?>

		<!-- button -->
		<?php _rock_button( $button ?? [] ); ?>

	</div>

	<!-- background image -->
	<?php _rock_cover_image( $bg_image ?? 0, 'banner' ); ?>

</header>
