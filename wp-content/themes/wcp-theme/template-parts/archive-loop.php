<?php
/**
 * Displays current loop for an archive
 *
 * @since   1.0.0
 * @package _rock
 */
extract( $meta );

$type = get_post_type();

$attrs = _rock_attrs( array(
	'id'    => $custom_id,
	'class' => array(
		'archive-loop',
		$custom_classes,
	),
));
?>

<section <?php echo $attrs; ?>>
	<div class="wrap">

		<?php if ( have_posts() ) : ?>

			<?php do_action( "_rock_before_{$type}_archive_loop" ); ?>

			<?php
			while ( have_posts() ) : the_post();

				get_template_part(
					'template-parts/content',
					is_search() ? 'search' : $type,
				);

			endwhile;
			?>

			<?php _rock_numbered_pagination(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>

	</div>
</section>
