<?php
/**
 * Footer template
 *
 * Contains the closing of the #content div and all content after.
 *
 * @since   1.0.0
 * @package _rock
 */
?>

	</div><!-- #content -->

	<footer role="contentinfo">

		<!-- main footer content -->
		<section class="footer-main">
			<div class="wrap">

				<!-- contact info and office hours -->
				<div class="left-content">
					<div class="contat-info">
						<?php _rock_svg( 'logo_winter-park-chiropractic' ); ?>
						<address>
							305 North Lakemont Ave.<br />
							Winter Park, FL 32792
						</address>

						<span class="footer-phone"><i class="fas fa-phone-square"></i> 407-339-2225</span>
					</div>

					<hr>

					<h2> Office Hours</h2>

					<ul class="office-hours">
						<li>
							Mon, Wed<br />
							8:00am-6:00pm
						</li>
						<li>
							Tue<br />
							10:00am to 2:00pm
						</li>
						<li>
							Thurs<br />
							2:00pm to 6:00pm
						</li>
						<li>
							Fri<br />
							8:00am - 2:00pm
						</li>
						<li>
							Sat, Sun<br />
							CLOSED
						</li>
						<li>
							Massages<br />
							offered daily
						</li>
					</ul>
				</div>

				<!-- footer menus -->
				<div class="right-content">
					<?php echo '<h2>Site Links</h2>'; ?>
					<?php wp_nav_menu([
						'theme_location' => 'primary',
						'menu_id'        => 'primary-menu',
						'depth'          => 3,
						'fallback_cb'    => false,
					]); ?>

					<?php echo '<h2>Services</h2>'; ?>
					<?php wp_nav_menu([
						'theme_location' => 'footer',
						'menu_id'        => 'footer-menu',
						'depth'          => 1,
						'fallback_cb'    => false,
					]); ?>
				</div>
			</div>
		</section>


		<!-- copyright -->
		<section class="copyright">
			<div class="wrap">
				&copy; <?php echo date( 'Y' ) . ' ' . __( 'All Rights Reserved. Winter Park Chiropractor.', '_rock' ); ?>
			</div>
		</section>

	</footer>

	<?php wp_footer(); ?>

</body>
</html>
