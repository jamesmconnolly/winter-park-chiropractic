<?php
/**
 * Main/Posts template file
 *
 * @since   1.0.0
 * @package _rock
 */
get_header(); ?>

	<?php _rock_section( 'banner' ); ?>

	<main class="wrap" role="main">

		<?php
		// hold off loop until "Archive Loop" row if there are row layouts
		if ( _rock_layouts()->has_rows() ) :

			_rock_layouts()->render();

		elseif ( have_posts() ) :

			while( have_posts() ) : the_post();

				get_template_part(
					'template-parts/content',
					is_search() ? 'search' : get_post_format()
				);

			endwhile;

			_rock_numbered_pagination();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main>

<?php get_footer();
