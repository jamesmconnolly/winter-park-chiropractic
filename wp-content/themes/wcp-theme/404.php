<?php
/**
 * 404 Page Template
 *
 * @since   1.0.0
 * @package _rock
 */
get_header(); ?>

	<?php _rock_section( 'banner' ); ?>

	<main role="main">
		<?php _rock_layouts()->render(); ?>
	</main>

<?php get_footer();
