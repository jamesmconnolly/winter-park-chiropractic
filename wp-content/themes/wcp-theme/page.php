<?php
/**
 * Default Page
 *
 * @since   1.0.0
 * @package _rock
 */

get_header(); ?>

<?php _rock_section( 'banner' ); ?>

	<main role="main">
		<?php
		while ( have_posts() ) : the_post();
			_rock_layouts()->render();
		endwhile;
		?>
	</main>

<?php get_footer();
