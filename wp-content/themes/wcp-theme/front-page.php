<?php
/**
 * Default Page
 *
 * @since   1.0.0
 * @package _rock
 */

get_header(); ?>

<!-- hero banner -->
<section class="bottom-angles-shadow">
	<div class="wpc-section hero-banner bottom-angles">
		<img src="<?php echo get_template_directory_uri();?>/assets/images/bg_dr-andrew-hull.jpg" alt="Dr Andrew Hull - Winter Park Chiropractor">
	</div>
</section>

<!-- section: intro -->
<section class="wpc-section doctor-intro angles-top-adjust">
	<div class="wrap">
		<?php
			echo '<h1>Meet Dr. Andrew J. Hull</h1>';
			echo '
				<p>Winter Park native Dr. Andrew J. Hull is a licensed, accredited physician with a degree from Palmer College of Chiropractic, where he was recognized among his colleagues for excelling in spinal manipulation.</p>

				<p>Because of his medical approach to chiropractic care, he is a highly referred physician trusted by his patients. Known for his gentle adjustments, especially with acute injuires, Dr. Hull has the ability to adapt to various body types, from NFL football players and professional baseball players to petite women and children. Once you meet Dr. Hull you will know why we were voted the Top Chiropractor in Orlando for 2016 and 2017.</p>
			';
		?>
	</div>
</section>

<!-- section: services -->
<section class="bottom-angles-shadow">
	<div class="wpc-section services bottom-angles">
		<div class="wrap">
			<?php echo '<h2 class="h1">Our Services</h2>'; ?>

			<ul class="services-blocks">
				<li class="service-single">
					<a href="#">
					<?php
						_rock_svg( 'services_chiropractic-techniques' );
						echo '<h3 class="h2">Chiropractic Techniques</h3>';
					?>
					</a>
				</li>
				<li class="service-single">
					<a href="#">
					<?php
						_rock_svg( 'services_workplace-discomfort' );
						echo '<h3 class="h2">Workplace Discomfort</h3>';
					?>
					</a>
				</li>
				<li class="service-single">
					<a href="#">
					<?php
						_rock_svg( 'services_auto-accidents-treatment' );
						echo '<h3 class="h2">Auto Accidents Treatment</h3>';
					?>
					</a>
				</li>
				<li class="service-single">
					<a href="#">
					<?php
						_rock_svg( 'services_science-based-nutrition' );
						echo '<h3 class="h2">Science Based Nutrition</h3>';
					?>
					</a>
				</li>
			</ul>
		</div>
	</div>
</section>

<!-- section: testimonials -->
<section class="wpc-section testimonials angles-top-adjust">
	<div class="wrap">
		<?php echo '<h2 class="h1">What People Are Saying</h2>'; ?>

		<!-- testimonial slider -->
		<div class="testimonial-slider">
			<div class="testimonial-single">
				<span class="star-rating">
					<i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i> <i class="fas fa-star"></i>
				</span>
				<?php echo '
					<p>Dr. Hull is wonderful and has great bedside manners. He takes time with each patient to make sure he is addressing any pain areas. I also reach out to him for any medical advice, not just questions on adjustments, and he helps or provides guidance whenever he can. I highly recommend him and the entire office staff.</p>

					<p>-V.Triplett</p>

				'; ?>
			</div>
			<div class="testimonial-slider-nav">
				<!-- navigation goes here -->
			</div>
		</div>

	</div>
</section>

<!-- section: partners -->
<section class="wpc-section partners">
	<div class="wrap">
		<?php echo '<h2 class="h1">Our Trusted Partners</h2>'; ?>

		<ul class="partners-grid">
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_standard-process.png" alt="Trusted Partner - Standard Process">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_si-jin-bao.png" alt="Trusted Partner - Si Jin Bao">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_core-products.png" alt="Trusted Partner - Core Products">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_foot-levelers.png" alt="Trusted Partner - Foot Levelers">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_cryoderm.png" alt="Trusted Partner - Cryoderm">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_biofreeze.png" alt="Trusted Partner - BioFreeze">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_biotics-research.png" alt="Trusted Partner - Biotics Research">
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri();?>/assets/images/partners_doterra.png" alt="Trusted Partner - Doterra">
			</li>
		</ul>
	</div>
</section>

<?php get_footer();