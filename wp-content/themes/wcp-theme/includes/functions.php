<?php
/**
 * Misc. helper functions
 *
 * @since   1.0.0
 * @package _rock
 */

/**
 * Are any of these things empty()?
 *
 * Checks all arguments to see if any of them are empty(). Alternative to:
 * if ( empty( this ) || empty( that ) ) {}.
 *
 * @param  mixed
 * @return bool
 *
 * @since  1.0.0
 */
function _rock_any_empty() {

	return ( count(
		array_filter(
			func_get_args(),
			function( $v ){ return empty( $v ); } // wrapper needed for empty()
		)
	) > 0 );
}


/**
 * Break array into string of attributes
 *
 * Has special cases for "class" and "data" keys if their values are arrays, so
 * it's compatible with arrays of data attributes (by using recusion).
 *
 * @param  array  $attrs   array of data attributes (keys) and their values
 * @param  string $prefix  a prefix for the data attribute (ex: "data-")
 * @return string          inline string of data attributes
 *
 * @since  1.0.0
 */
function _rock_attrs( $attrs, $prefix = '' ) {

	foreach ( $attrs as $attr => &$value ) {

		// remove if empty
		if ( empty( $value ) ) {
			unset( $attrs[ $attr ] );
			continue;
		}

		// if data- attributes
		if ( $attr == 'data' && is_array( $value ) ) {
			$attrs[ $attr ] = _rock_attrs( array_filter( $value ), 'data-' );
			continue;
		}

		// if array of classes
		if ( $attr == 'class' && is_array( $value ) ) {
			$value = implode( ' ', array_filter( $value ) );
		}

		// array of classes + all other cases
		$attrs[ $attr ] = $prefix . $attr . '="' . esc_attr( $value ) . '"';
	}

	return apply_filters( '_rock_attrs', implode( ' ', $attrs ), $attrs );
}


/**
 * Get/build a button
 *
 * @see   _Rock_Button::get_button
 * @since 1.0.0
 */
function _rock_get_button( $button, $query = [], $data = [] ) {
	return _Rock_Button::get_button( $button, $query, $data );
}


/**
 * Output a button
 *
 * @since 1.0.0
 */
function _rock_button( $button, $query = [], $data = [] ) {
	echo _rock_get_button( $button, $query, $data );
}


/**
 * Get a piece of contact information
 *
 * @see    class-contacts/get_contact
 * @since  1.0.0
 */
function _rock_get_contact( $field, $group = 'orl' ) {
	return _Rock_Contacts::get_contact( $field, $group );
}


/**
 * Output a piece of contact information
 *
 * @see   _rock_get_contact
 * @since 1.0.0
 */
function _rock_contact( $field, $group = 'orl' ) {
	echo _rock_get_contact( $field, $group );
}


/**
 * Get an address
 *
 * @see  class-contacts/get_address
 * @since 1.0.0
 */
function _rock_get_address( $group = 'orl', $html = true, $link = true ) {
	return _Rock_Contacts::get_address( $group, $html, $link );
}


/**
 * Output an address
 *
 * @see   _rock_get_address
 * @since 1.0.0
 */
function _rock_address( $group = 'orl', $html = true, $link = true ) {
	echo _rock_get_address( $group, $html, $link );
}


/**
 * Output WYSIWYG content into a div
 *
 * @param string  $content
 * @since 1.0.0
 */
function _rock_entry_content( $content ) {
	echo $content ? '<div class="entry-content">' . $content . '</div>' : '';
}


/**
 * Get an SVG file so it can be used inline
 *
 * @param  string  $name   the file name of the svg in the theme's images folder, minus .php
 * @param  class   $class  an extra class to add to the wrapper
 * @return string          contents of SVG file
 *
 * @since  1.0.0
 */
function _rock_get_svg( $name, $class = null ) {

	if ( empty( $name ) || ! is_string( $name ) ) {
		return;
	}

	// config file location
	$svg = _ROCK_DIR . '/assets/images/' . $name . '.svg';

	// make sure something is there
	if ( ! file_exists( $svg ) ) {
		return;
	}

	$class = _rock_attrs([ 'class' => [
		'svg', $name, $class,
	]]);

	// get the file
	ob_start();
	include $svg;
	return "<div $class>" . ob_get_clean() . "</div>";
}


/**
 * Output an inlined SVG
 *
 * @see   _rock_get_svg() for params
 * @since 1.0.0
 */
function _rock_svg( $name, $class = null ) {
	echo _rock_get_svg( $name, $class );
}


/**
 * Get current page template filename without the ".php"
 *
 * @return string         current page template filename without .php
 * @return bool    false  template not found
 *
 * @since  1.0.0
 */
function _rock_get_page_template() {

	$template = basename( get_page_template() );

	if ( $template ) {
		return str_replace( '.php', '', $template );
	} else {
		return false;
	}
}


/**
 * Get a video src from an iframe
 *
 * @param  string  $iframe  iframe code returned by ACF
 * @return string           video URL / src attribute with autoplay
 *
 * @see    https://developers.google.com/youtube/player_parameters
 * @since  1.0.0
 */
function _rock_get_iframe_src( $iframe, $autoplay = true, $controls = true ) {

	// match the src attribute
	preg_match( '/src="(.+?)"/', $iframe, $matches );

	// sanity check
	if ( empty( $matches[1] ) ) return '';

	return add_query_arg([
		'autoplay'       => (int) $autoplay,
		'controls'       => (int) $controls,
		'modestbranding' => 1,
		'rel'            => 0,
		'fs'             => 0,
		'color'          => 'white',
	], $matches[1] );
}


/**
 * Sanitize a string or single-dimensional array
 *
 * @param  mixed  $value  string or array to sanitize
 * @return mixed          array or string with 99.9% less germs
 *
 * @since  1.0.0
 */
function _rock_array_sanitize( $value ) {

	if ( gettype( $value ) == 'string' ) {
		return sanitize_text_field( $value );
	}

	// call again if array
	elseif ( is_array( $value ) ) {
		return array_map( __FUNCTION__, $value );
	}

	else {
		throw new Error( '_rock_array_sanitize is for arrays and strings only' );
	}
}


/**
 * Get a meta/customizer-friendly key name
 *
 * @param  string  $name
 * @return string
 *
 * @since 1.0.0
 */
function _rock_format_key_name( $name ) {
	return str_replace( '-', '_', strtolower( sanitize_file_name( $name ) ) );
}


/**
 * Lower Yoast SEO meta box priority
 *
 * @return string  meta box priority
 * @since  1.0.0
 */
function _rock_set_yoast_priority() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', '_rock_set_yoast_priority' );


/**
 * Load a section template part with an ACF Field Group
 *
 * Just pass the slug and name of the template part you need, specifying a clone
 * field's name as necessary.
 *
 * Meta is filtered with these hooks, with ACF field name and additional args
 * passed to the callbacks:
 * @see meta/{slug}
 * @see meta/{slug}/{$name}
 *
 *
 * @param string             $slug        slug name for the generic template
 * @param string|null        $name        name of specialized template
 * @param string|array|null  $meta_field  An array of meta, or the (clone) field name. if
 *                                        meta is passed via $args, that takes priority.
 * @param array|null         $args        Additional data to extract()
 *
 * @since 1.0.0
 */
function _rock_section( $slug, $name = null, $meta_field = null, $args = [] ) {

	do_action( "_rock_section_{$slug}", $slug, $name );

	// auto-assume options page ID equaling a CPT key for an archive!
	$post_id = is_archive() ? get_post_type() : get_the_ID();
	$post_id = apply_filters( '_rock_section_post_id', $post_id, $slug, $name );

	// no meta passed (through args)? check field name.
	if ( empty( $args['meta'] ) ) {

		// if $meta_field is array, use that
		if ( is_array( $meta_field ) ) {
			$args['meta'] = $meta_field;
		}

		// if a string (for a field name), check for the field. Fall back to an
		// array if not found.
		elseif ( is_string( $meta_field ) ) {
			$args['meta'] = $meta_field ? ( get_field( $meta_field, $post_id ) ?: [] ) : [];
		}

		// if there still isn't anything, try to match a field with the slug + _name
		elseif ( ! $meta_field ) {
			$field_name   = _rock_format_key_name( $slug . ( $name ? "_$name" : '' ) );
			$args['meta'] = get_field( $field_name, $post_id ) ?: [];
		}

		// if all else fails, default empty
		else {
			$args['meta'] = [];
		}
	}


	// apply meta filters
	$args['meta'] = apply_filters( "meta/{$slug}"        , $args['meta'], $meta_field, $args );
	$args['meta'] = apply_filters( "meta/{$slug}/{$name}", $args['meta'], $meta_field, $args );


	// load template part
	_rock_template_part( $slug, $name, $args );
}


/**
 * Get a template part from the theme
 *
 * @param string  $slug  the slug name for the generic template
 * @param string  $name  the name of the specialized template
 * @param array   $args  arguments passed to _rock_load_template
 *
 * @since 1.0.0
 */
function _rock_template_part( $slug, $name = null, $args = [] ) {

	// allow edge cases to get in here
	do_action( "get_template_part_{$slug}", $slug, $name );

	// build the path
	$path = _ROCK_DIR . '/template-parts/' . $slug;

	// add specialized name
	$path .= $name ? "-$name" : '';

	$path .= '.php';

	// check for the file
	if ( file_exists( $path ) ) {
		_rock_load_template( $path, false, $args );
	}
}


/**
 * Load template
 *
 * Modified version of WordPress' load_template function with an added argument
 * to pass variables to the required template part to avoid needing
 * memory-hogging global vars.
 *
 * This implementation is similar to what WooCommerce does.
 *
 * @param string  $_template_file  path to the template file
 * @param bool    $require_once    whether to require_once or require
 * @param array   $args            variables extracted to template part
 *
 * @since 1.0.0
 */
function _rock_load_template( $_template_file, $require_once = true, $args = [] ) {

	global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

	if ( is_array( $wp_query->query_vars ) ) {
		extract( $wp_query->query_vars, EXTR_SKIP );
	}

	// modification
	if ( is_array( $args ) ) {
		extract( $args, EXTR_SKIP );
	}

	if ( isset( $s ) ) {
		$s = esc_attr( $s );
	}

	if ( $require_once ) {
		require_once( $_template_file );
	} else {
		require( $_template_file );
	}
}


/**
 * Output numbered pagination
 *
 * @since 1.0.0
 */
function _rock_numbered_pagination() {

	global $wp_query;

	$big   = 999999999; // need an unlikely integer
	$links = paginate_links([
		'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'             => '?paged=%#%',
		'current'            => max( 1, get_query_var( 'paged' ) ),
		'total'              => $wp_query->max_num_pages,
		'mid_size'           => 2,
		'end_size'           => 2,
		'prev_text'          => __( 'Previous', '_rock' ),
		'next_text'          => __( 'Next', '_rock' ),
		'type'               => 'list',
		'before_page_number' => '<span class="screen-reader-text">' . __( 'Page', '_rock' ) . ' </span>',
	]);

	echo '<nav class="posts-navigation numbered-pagination">' . $links . '</nav>';
}

// let plugins hook in safely
add_action( '_rock_numbered_pagination', '_rock_numbered_pagination' );


/**
 * Add breakpoint testing
 *
 * @since 1.0.0
 */
function _rock_add_breakpoint_test() { ?>
	<!--<div class="breakpoint-test"><span class="name"></span><span class="size"></span></div>-->
<?php }
add_action( 'wp_footer', '_rock_add_breakpoint_test', 100 );


/**
 * Let the admin know if SOIL isn't active
 *
 * @since 1.0.0
 */
function _rock_add_soil_notice() {

	if ( class_exists( '\\Roots\soil\\Options' ) ) {
		return;
	}

	$message = sprintf(
		__( 'This theme requires the %sSoil%s plugin to work properly!' ),
		'<b><a href="https://github.com/roots/soil/releases" target="_BLANK">', '</a></b>'
	);

	echo '<div class="notice notice-error"><p>' . $message . '</p></div>';

}
add_action( 'admin_notices', '_rock_add_soil_notice' );


/**
 * Is the site currently in production?
 *
 * Checks for wp-config-local 'DZ_IS_PROD' definition or live Pantheon
 * environment super
 *
 * @return string   'pantheon'  Pantheon "live" environment detected
 * @return string   'local'     'DZ_IS_PROD' defined in wp-config-local.php
 * @return boolean  false       neither production variable found
 *
 * @since  1.0.0
 */
function _rock_is_prod() {

	if ( defined( 'DZ_IS_PROD' ) && DZ_IS_PROD ) {
		return 'local';
	} elseif ( isset( $_ENV['PANTHEON_ENVIRONMENT'] ) && $_ENV['PANTHEON_ENVIRONMENT'] === 'live' ) {
		return 'pantheon';
	} else {
		return false;
	}
}


/**
 * Get the label for the production environment
 *
 * @return string   how it's determined as a "production" environment
 * @return boolean  false  not a production environment
 *
 * @since 1.0.0
 */
function _rock_get_prod_label() {

	switch ( $prod = _rock_is_prod() ) {

		case 'local' :
			return __( 'Local config', '_rock' );
			break;

		case 'pantheon' :
			return __( 'Live Pantheon Environment', '_rock' );
			break;

		default :
			return false;
			break;
	}
}


/**
 * Link DZ and notify of production environment in admin footer
 *
 * @param  string  existing footer text
 *
 * @return string
 * @since  1.0.0
 */
function _rock_set_admin_footer( $text ) {

	$site_by = sprintf(
		__( 'Site by %sDesignzillas%s.', '_rock' ),
		'<a href="https://www.designzillas.com/" target="_BLANK">', '</a>'
	);

	$prod = _rock_get_prod_label();
	$prod = $prod ? sprintf(
		__( '%sProduction%s', '_rock' ),
		'<b><abbr title="' . esc_attr( $prod ) . '">', '</abbr></b>'
	) . ' ' : '' ;

	return ( $text ? "$text | " : '' ) . $prod . $site_by;
}
add_filter( 'admin_footer_text', '_rock_set_admin_footer' );

/**
 * Get a relevant "post ID", considering the current page, archive, etc.
 *
 * Auto-assume an options page ID equaling the current CPT key for an archive,
 * then allow filtering it.
 *
 * @return integer|string  $post_id
 * @since  1.0.0
 */
function _rock_get_post_or_options_id() {
	$post_id = is_archive() ? get_post_type() : get_the_ID();
	return apply_filters( '_rock_get_post_id', $post_id );
}

/**
 * Get a section as a string
 *
 * @return string  section HTML
 *
 * @see    _rock_section() for params
 * @since  1.0.0
 */
function _rock_get_section( $slug, $name = null, $meta_field = null, $args = array() ) {
	ob_start();
	_rock_section( $slug, $name, $meta_field, $args );
	return trim( ob_get_clean() );
}

/**
 * Disable Rank Math's sitemap cache.
 *
 * @since 1.0.0
 */
add_filter( 'rank_math/sitemap/enable_caching', '__return_false' );
