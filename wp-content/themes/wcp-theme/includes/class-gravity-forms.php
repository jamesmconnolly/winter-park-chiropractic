<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Gravity Forms
 *
 * Misc Gravity Forms modifications/helpers
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Gravity_Forms {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'gform_init_scripts_footer' , '__return_true'                               );
		add_filter( 'gform_submit_button'       , [ __CLASS__ , 'set_submit_button'    ], 10, 2 );
		add_filter( 'gform_ajax_spinner_url'    , [ __CLASS__ , 'set_spinner_url'      ]        );
		add_filter( 'gform_field_css_class'     , [ __CLASS__ , 'add_field_type_class' ], 10, 2 );
		add_filter( 'gform_field_content'       , [ __CLASS__ , 'add_autocomplete_off' ], 10, 2 );
	}


	/**
	 * Apply the theme's button component to the Gravity Form submit
	 *
	 * This allows us to have a non-"input" DOM element that can be customized
	 * at the full capacity of our button function.
	 *
	 * @param  string  $button  existing Gravity Forms button HTML
	 * @param  array   $form    Gravity Form information
	 * @return string           our own button
	 *
	 * @since 1.0.0
	 */
	public static function set_submit_button( $button, $form ) {

		// get form ID & text
		$id   = $form['id'];
		$text = ! empty( $form['button']['text'] ) ? $form['button']['text'] : __( 'Submit', '_rock' );

		return _rock_get_button( array(
			'tag'      => 'button',
			'type'     => 'submit',
			'value'    => 'submit',
			'id'       => "gform_submit_button_$id",
			'text'     => $text,
			'icon'     => 'check',
			'tabindex' => GFCommon::$tab_index++,
		));
	}


	/**
	 * Set the AJAX spinner URL
	 *
	 * Get a spinner here (or something): https://loading.io
	 *
	 * @since 1.0.0
	 */
	public static function set_spinner_url() {
		return _ROCK_DIR_URI . '/assets/images/ajax.svg';
	}


	/**
	 * Add Gravity Form field type to field wrapper class(es)
	 *
	 * @param  string  $classes  existing classes, separated by space
	 * @param  object  $field    field object
	 * @return string            new class(es)
	 *
	 * @since 1.0.0
	 */
	public static function add_field_type_class( $classes, $field ) {
		return $classes .= ' gfield_' . $field->get_input_type();
	}


	/**
	 * Turn "autocomplete" off on certain field types
	 *
	 * @param  string  $field_content  field HTML
	 * @param  object  $field          field object
	 * @return string                  new HTML
	 *
	 * @since  1.0.0
	 */
	public static function add_autocomplete_off( $field_content, $field ) {

		if ( empty( $field->dateType ) || $field->dateType !== 'datepicker' ) {
			return $field_content;
		}

		return str_replace( "type=", "autocomplete='off' type=", $field_content );
	}
}

new _Rock_Gravity_Forms;
