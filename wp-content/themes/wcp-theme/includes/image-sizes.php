<?php
/**
 * Custom image sizes
 *
 * @link    https://developer.wordpress.org/reference/functions/add_image_size/
 * @since   1.0.0
 * @package _rock
 */
function _rock_add_image_sizes() {
	add_image_size( 'banner' , 1650 , 1200 );
}
add_action( 'init', '_rock_add_image_sizes', 20 );
