<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Blog ("post" archive) Setup
 *
 * Set post_id for _section, get stuff, etc.
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Blog {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( '_rock_section_post_id' , [ __CLASS__ , 'set_post_id'      ] );
		add_filter( 'excerpt_more'          , [ __CLASS__ , 'set_excerpt_more' ] );
	}


	/**
	 * Get the blog page ID
	 *
	 * @return integer  "posts page" ID
	 * @since  1.0.0
	 */
	public static function get_page_id() {
		return get_option( 'page_for_posts' );
	}


	/**
	 * Get blog/news URL
	 *
	 * @return string
	 * @since  1.0.0
	 */
	public static function get_url() {
		return get_post_type_archive_link( 'post' );
	}


	/**
	 * Output the blog URL
	 *
	 * @since 1.0.0
	 */
	public static function the_url() {
		echo self::get_url();
	}


	/**
	 * Set the post ID for blog page sections
	 *
	 * @param  mixed  $post_id  existing post_id to switch to blog (if needed)
	 * @return mixed            existing post_id or blog page options post_id
	 *
	 * @since  1.0.0
	 */
	public static function set_post_id( $post_id ) {
		return is_home() ? self::get_page_id() : $post_id;
	}


	/**
	 * Modify the "more" elipses and add a button
	 * to the post
	 *
	 * @param   string  $more  existing "more" text
	 * @return  string         new "more" with ... and button
	 *
	 * @since   1.0.0
	 */
	public static function set_excerpt_more( $more ) {

		return ' ...' . _rock_get_button([
			'text' => __( 'Continue Reading', '_rock' ),
			'url'  => get_the_permalink(),
		]);
	}
}

new _Rock_Blog;
