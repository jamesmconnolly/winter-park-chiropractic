<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Script/style handling
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Scripts {

	/**
	 * Asset location
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $assets = _ROCK_DIR_URI . '/assets/';


	/**
	 * Hook things
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'wp_enqueue_scripts' , [ __CLASS__ , 'add_scripts'        ]         );
		add_action( 'script_loader_tag'  , [ __CLASS__ , 'maybe_defer_script' ] , 10, 2 );
		add_action( 'wp_footer'          , [ __CLASS__ , 'add_google_fonts'   ]         );
	}


	/**
	 * Enqueue scripts and styles
	 *
	 * @since 1.0.0
	 */
	public static function add_scripts() {

		$template = _rock_get_page_template();

		// Dequeue unused block editor styles
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );

		// main CSS
		wp_enqueue_style( '_rock', self::$assets . 'css/style.min.css', [], _ROCK_THEME_VERSION, 'all' );

		// styleguide dev
		if ( $template == 'styleguide' ) {
			wp_enqueue_style( '_rock-styleguide', self::$assets . 'css/styleguide.min.css', [ '_rock' ], _ROCK_THEME_VERSION, 'all' );
		}

		// foot scripts
		wp_enqueue_script( '_rock-foot', self::$assets . 'js/foot.min.js', [ 'jquery' ], _ROCK_THEME_VERSION, true );

		// head scripts - only use if absolutely needed!
		// wp_enqueue_script( '_rock-head', $assets . 'js/head.min.js', [], _ROCK_THEME_VERSION, false );

		/**
		 * Font Awesome (JS): We're using the "free pro" icons by default. Keep the
		 * version number up to date. The "maybe defer" filter can probably be
		 * removed if a "pro" pack needs to be downloaded.
		 *
		 * Make sure you update the icon() mixin and button component to use the
		 * the pack being enqueued. They're "solid" by default, like this CDN.
		 *
		 * @see https://fontawesome.com/get-started/svg-with-js
		 */
		wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.6/js/all.js' );

		// comments
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}


	/**
	 * Filter HTML script tag to MAYBE add the "defer" attribute
	 *
	 * @param  string  $tag     existing <script> tag
	 * @param  string  $handle  registered handle
	 * @return string  $tag     new <script> with defer
	 *
	 * @since  1.0.0
	 */
	public static function maybe_defer_script( $tag, $handle ) {

		$allow = [ 'font-awesome' ];

		if ( in_array( $handle, $allow ) ) {
			$tag = str_replace( ' src', ' defer src', $tag );
		}

		return $tag;
	}


	/**
	 * Load Google Fonts
	 *
	 * @since 1.0.0
	 */
	public static function add_google_fonts() { ?>

		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
		<script>
			WebFont.load({
				google : {
					families : [
						'Raleway:500,600'
					]
				}
			});
		</script>

	<?php }
}

new _Rock_Scripts;
