<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Social media utility class
 *
 * Configure sections and controls for the theme in the customizer
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Socials {

	/**
	 * Social definitions
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $profiles;


	/**
	 * Profile URLs
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $urls;


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'pre_get_posts' , [ __CLASS__ , 'add_customizer_section' ] );
	}


	/**
	 * Add social profiles to list
	 *
	 * @param  array  $profiles  labels and their Font Awesome icon
	 * @since  1.0.0
	 */
	public static function add_profiles( $profiles ) {
		array_walk( $profiles, [ __CLASS__ , 'add_profile' ] );
	}


	/**
	 * Add social profile to list
	 *
	 * @param   string  $fa    FontAwesome icon suffix (ex: facebook-official)
	 * @param   string  $name  profile name (ex: Facebook)
	 * @return  array          array element that was set
	 *
	 * @since   1.0.0
	 */
	public static function add_profile( $fa = '', $name ) {

		// generate key name
		$key = _rock_format_key_name( $name );

		// set name
		$profile['name'] = $name;

		// set icon
		if ( $fa ) {
			$profile['fa'] = $fa;
		}

		// add it
		return self::$profiles[ $key ] = $profile;
	}


	/**
	 * Add customizer section for profiles
	 *
	 * @since 1.0.0
	 */
	public static function add_customizer_section() {

		if ( empty( self::$profiles ) ) {
			return;
		}

		foreach ( self::$profiles as $key => $data ) {

			$controls[ $key ] = array(
				'type'	=> 'url',
				'label'	=> $data['name'],
			);
		}

		return _Rock_Customizer::add_section( __( 'Social Profiles', '_rock' ), $controls );
	}


	/**
	 * Get profile URLs from customizer
	 *
	 * @since 1.0.0
	 */
	private static function get_profile_urls() {

		// bounce if no profiles or we already got urls
		if ( empty( self::$profiles ) || ! empty( self::$urls ) ) {
			return;
		}

		// set the url in ->urls and ->profiles
		foreach ( self::$profiles as $key => &$data ) {
			$url = esc_url( get_theme_mod( $key ) );
			self::$urls[ $key ] = $url;
			self::$profiles[ $key ]['url'] = $url;
		}
	}


	/**
	 * Get Profiles
	 *
	 * Return an array of the social profiles and their data
	 *
	 * @return bool   false  none found
	 * @return array         the profiles
	 *
	 * @since  1.0.0
	 */
	public static function get_profiles() {

		if ( empty( $self::profiles ) ) {
			return;
		}
		$self::get_profile_urls();

		return self::$profiles;
	}


	/**
	 * Get icon nav
	 *
	 * @return string  $nav   navigation menu with icon links
	 *
	 * @since  1.0.0
	 */
	public static function get_icon_nav() {

		// make sure there's URLs
		self::get_profile_urls();
		if ( empty( self::$urls ) ) {
			return;
		}

		$nav = '';

		foreach ( self::$profiles as $key => $data ) {

			// skip if no URL or icon
			if ( empty( $data['url'] || empty( $data['fa'] ) ) ) {
				continue;
			}

			// set link
			$icon = '<i class="fab fa-' . $data['fa'] . '"></i>';
			$name = $data['name'] ? $data['name'] : $key;
			$url  = $data['url'];
			$link = "<a href='$url' target='_blank'>$icon$name</a>";

			$socials[] = $link;
		}

		if ( empty( $socials ) ) {
			return;
		}

		// wrap links and return
		return '<nav class="socials">' . implode( '', $socials ) . '</nav>';
	}


	/**
	 * Output the icon navigation
	 *
	 * @since 1.0.0
	 */
	public static function icon_nav() {
		echo self::get_icon_nav();
	}
}

new _Rock_Socials;
