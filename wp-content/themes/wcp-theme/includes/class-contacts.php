<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Contact information utility
 *
 * Configure sections and controls for contact information in the customizer
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Contacts {

	/**
	 * Customizer control/setting ID prefix
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $prefix = 'contact';


	/**
	 * Information groups
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $groups = [];


	/**
	 * Field cache
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $cache = [];


	/**
	 * Address cache
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $addresses = [];

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'pre_get_posts'           , [ __CLASS__ , 'add_customizer_section' ] );
		add_filter( '_rock_get_contact_phone' , 'esc_attr'                               );
		add_filter( '_rock_get_contact_email' , 'sanitize_email'                         );
	}


	/**
	 * Register a group of contact details
	 *
	 * @param string  $label  Contact group label (ex: US Office)
	 * @param string  $key    unique ID for group
	 *
	 * @since 1.0.0
	 */
	public static function add_group( $label, $key = 'orl' ) {
		self::$groups[] = [ $key, $label ];
	}


	/**
	 * Add the customizer section
	 *
	 * @since 1.0.0
	 */
	public static function add_customizer_section() {

		if ( empty( self::$groups ) ) {
			return;
		}

		foreach ( self::$groups as $group ) {
			$sections[] = [ $group[1], self::get_controls( $group[0] ) ];
		}

		if ( count( $sections ) > 1 ) {
			_Rock_Customizer::add_panel( __( 'Contact Information', '_rock' ), $sections );
		} else {
			_Rock_Customizer::add_section( __( 'Contact Information', '_rock' ), $sections[0][1] );
		}
	}


	/**
	 * Get controls/fields for a customizer section
	 *
	 * @param  string  $group  group prefix for customizer setting/control
	 * @return array
	 *
	 * @since 1.0.0
	 */
	private static function get_controls( $group = false ) {

		$key = $group ? self::$prefix . "_{$group}_" : '';

		return [
			"{$key}phone"         => [
				'label'       => __( 'Phone Number', '_rock' ),
				'description' => __( 'For tel: link. Do not include +1-', '_rock' ),
			],
			"{$key}phone_display" => [
				'label'       => __( 'Phone Display', '_rock' ),
				'description' => __( 'What users see. Ex: (123) 456-7890', '_rock' ),
			],
			"{$key}email"         => __( 'Email', '_rock' ),
			"{$key}addr_1"        => __( 'Address Line 1', '_rock' ),
			"{$key}addr_2"        => __( 'Address Line 2', '_rock' ),
			"{$key}city"          => __( 'City', '_rock' ),
			"{$key}state"         => __( 'State', '_rock' ),
			"{$key}zip"           => __( 'ZIP', '_rock' ),
		];
	}


	/**
	 * Get a piece of contact information
	 *
	 * Save to the cache if it's the first time we're trying to get it.
	 *
	 * @param  string  $field
	 * @return mixed
	 *
	 * @since  1.0.0
	 */
	public static function get_contact( $field, $group = 'orl' ) {

		if ( isset( self::$cache[ $group ][ $field ] ) ) {
			return self::$cache[ $group ][ $field ];
		}

		$key = self::$prefix . "_{$group}_{$field}";

		return self::$cache[ $group ][ $field ] = apply_filters(
			"_rock_get_contact_$field",
			get_theme_mod( $key )
		);
	}


	/**
	 * Build an address
	 *
	 * @param  string   $group
	 * @param  boolean  $html    include HTML DOM
	 * @param  boolean  $link    Add Google Maps link
	 * @return string   $output
	 *
	 * @since  1.0.0
	 */
	public static function get_address( $group = 'orl', $html = true, $link = true ) {

		if ( ! $lines = self::get_address_lines( $group ) ) {
			return '';
		}

		// HTML?
		$output = $lines;
		$output = $html
			? '<address>' . implode( '<br>', $output ) . '</address>'
			: implode( ' ', $output );

		// link?
		if ( $link ) {

			$link = _rock_attrs([
				'href'   => 'https://www.google.com/maps/place/' . urlencode( implode( ' ', $lines ) ),
				'target' => '_blank',
				'title'  => __( 'See in Google Maps', '_rock' ),
			]);
			$output = "<a $link>$output</a>";
		}

		return $output;
	}


	/**
	 * Get address lines
	 *
	 * @param  string   $group
	 * @return array            lines for address
	 *
	 * @since  1.0.0
	 */
	private static function get_address_lines( $group ) {

		if ( isset( self::$addresses[ $group ] ) ) {
			return self::$addresses[ $group ];
		}

		$city    = self::get_contact( 'city'  , $group );
		$state   = self::get_contact( 'state' , $group );
		$zip     = self::get_contact( 'zip'   , $group );

		$lines = array_filter([
			'addr_1' => self::get_contact( 'addr_1', $group ),
			'addr_2' => self::get_contact( 'addr_2', $group ),
			'addr_3' => trim( $city . ( $city && $state ? ', ' : '' ) . $state . ' ' . $zip ),
		]);

		return self::$addresses[ $group ] = empty( $lines )
			? false
			: $lines;
	}


	/**
	 * Get all contacts for a group
	 *
	 * @param  string  $group  group ID, as used when adding it
	 * @since 1.0.0
	 */
	public static function get_group( $group = 'orl' ) {

		foreach ( self::get_controls() as $key => $label ) {
			$contacts[ $key ] = self::get_contact( $key, $group );
		}

		$contacts['address'] = self::get_address( $group, true, true );

		return $contacts;
	}


	/**
	 * Get all contact groups
	 *
	 * @return array
	 * @since  1.0.0
	 */
	public static function get_groups() {

		foreach ( self::$groups as $group ) {
			$groups[] = self::get_group( $group[0] );
		}

		return $groups;
	}
}

new _Rock_Contacts;
