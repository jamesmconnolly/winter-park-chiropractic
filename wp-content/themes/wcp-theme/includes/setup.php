<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since   1.0.0
 * @package _rock
 */
function _rock_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on _rock, use a find and replace
	 * to change '_rock' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( '_rock', _ROCK_DIR . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus([
		'primary' => esc_html__( 'Primary', '_rock' ),
		'footer'  => esc_html__( 'Footer', '_rock' )
	]);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', [
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	]);

	/*
	 * Add Root's Soil Support
	 */
	add_theme_support( 'soil-clean-up' );
	add_theme_support( 'soil-disable-trackbacks' );
	add_theme_support( 'soil-nav-walker' );
	add_theme_support( 'soil-nice-search' );
}
add_action( 'after_setup_theme', '_rock_setup' );



/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global integer  $content_width
 */
function _rock_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_rock_content_width', 1200 );
}
add_action( 'after_setup_theme', '_rock_content_width', 0 );
