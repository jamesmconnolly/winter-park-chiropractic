<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * ACF extensions, such as WYSIWYG toolbar adjustments
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_ACF {

	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_filter( 'acf/init'                    , [ __CLASS__ , 'set_google_api_key'   ] );
		add_filter( 'acf/fields/wysiwyg/toolbars' , [ __CLASS__ , 'set_wysiwyg_toolbars' ] );
		add_filter( 'acf/settings/show_admin'     , [ __CLASS__ , 'maybe_disable_admin'  ] );
	}


	/**
	 * Set Google API key
	 *
	 * @since 1.0.0
	 */
	public static function set_google_api_key() {
		acf_update_setting( 'google_api_key', _ROCK_GAPI );
	}


	/**
	 * Set WYSIWYG field toolbars
	 *
	 * @param  array  $toolbars  existing WYSIWYG toolbar fields
	 * @return array  $toolbars
	 *
	 * @since  1.0.0
	 */
	public static function set_wysiwyg_toolbars( $toolbars ) {

		// core "basic" options
		$basic = array(
			'bold', 'italic', 'underline', 'strikethrough',
			'undo', 'redo', 'link', 'unlink',
		);
		$toolbars['Very Basic'][1] = $basic;

		// additional "with headings and lists" options
		array_unshift( $basic, 'formatselect' );
		$basic[] = 'bullist';
		$basic[] = 'numlist';
		$toolbars['Very Basic with Headings and Lists'][1] = $basic;

		return $toolbars;
	}


	/**
	 * Disable ACF admin if production environment
	 *
	 * @return boolean
	 * @since  1.0.0
	 */
	public static function maybe_disable_admin() {
		return ! _rock_is_prod();
	}
}

new _Rock_ACF;
