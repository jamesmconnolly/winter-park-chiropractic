<?php

// exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Flexible content layout routing/functionality
 *
 * @since   1.0.0
 * @package _rock
 */
class _Rock_Layouts {

	/**
	 * ACF flex content field key
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	const FIELD_KEY = 'layouts';


	/**
	 * Post ID for layouts
	 *
	 * @var   integer
	 * @since 1.0.0
	 */
	private $post_id;


	/**
	 * Layout HTMLs
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private $html = array();


	/**
	 * Pre-header layout HTMLs
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private $pre_html = array();


	/**
	 * Layout asset dependencies
	 *
	 * @var   array
	 * @since 1.0.0
	 */
	private static $deps = array(

		// ex:
		// 'featherlight' => array(
		// 	'type'	=> 'js',
		// 	'src'	=> 'https://cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js',
		// 	'deps'	=> array( 'jquery' ),
		// ),
	);


	/**
	 * Custom body classes to add
	 *
	 * @var array
	 * @since 1.0.0
	 */
	private static $body_classes = array();


	/**
	 * ACF field setting name for "Flex Content Only"
	 *
	 * @var   string
	 * @since 1.0.0
	 */
	private static $setting_flex_only = 'flex_content_only';


	/**
	 * Singleton instance
	 *
	 * @var   _Rock_Layouts
	 * @since 1.0.0
	 */
	private static $instance;


	/**
	 * Get things going
	 *
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'wp'                          , [ $this, 'set_id'                ], 8  );
		add_action( 'wp'                          , [ $this, 'get_layouts'           ]     );
		add_action( 'wp_enqueue_scripts'          , [ $this, 'set_deps'              ]     );
		add_action( 'acf/render_field_settings'   , [ $this, 'add_field_settings'    ]     );
		add_action( 'acf/prepare_field'           , [ $this, 'maybe_kill_flex_field' ]     );
		add_filter( 'body_class'                  , [ $this, 'add_body_classes'      ], 15 );

		// testing!
		// add_action( 'wp_footer', function() { d( $this, null, null ); } );
	}


	/**
	 * Set post ID for the current post's layouts
	 *
	 * @since 1.0.0
	 */
	public function set_id() {
		return $this->post_id = _rock_get_post_or_options_id();
	}


	/**
	 * Does the current post or archive have layout rows?
	 *
	 * @return boolean
	 * @since  1.0.0
	 */
	public function has_rows() {
		return have_rows( self::FIELD_KEY, $this->post_id );
	}


	/**
	 * Load Layouts
	 *
	 * Loop through the layouts we have and load the template part for each one
	 *
	 * @since 1.0.0
	 */
	public function get_layouts() {

		// loop the rows, getting their layout & deps
		while ( have_rows( self::FIELD_KEY, $this->post_id ) ) { the_row();
			$this->get_layout( get_row_layout() );
		}
	}


	/**
	 * Load layout
	 *
	 * @param  string  $layout  flex field layout name
	 * @return bool
	 *
	 * @since  1.0.0
	 */
	private function get_layout( $layout ) {

		$meta = get_row( true );

		// check field hider!
		if ( isset( $meta['hide'] ) && $meta['hide'] === true ) {
			return;
		}

		// get proper layout name
		$layout    = explode( '-', $layout, 2 );
		$layout[1] = empty( $layout[1] ) ? '' : $layout[1];

		// get layout DOM from template-part
		$html = _rock_get_section( $layout[0], $layout[1], null, array( 'meta' => $meta ) );

		// check if it's a pre-header layout
		if ( ! empty( get_sub_field( 'pre_header' ) ) ) {
			$this->pre_html[] = $html;
		} else {
			$this->html[] = $html;
		}
	}


	/**
	 * Add an asset dependency
	 *
	 * Called by layouts
	 *
	 * @param string  $handle  script/style handle for wp_enqueue_...
	 * @since 1.0.0
	 */
	public static function add_dep( $handle ) {
		self::$deps[ $handle ]['needed'] = true;
	}


	/**
	 * Enqueue layout asset dependencies
	 *
	 * @since 1.0.0
	 */
	public static function set_deps() {

		foreach ( self::$deps as $handle => $args ) {

			if ( empty( $args['needed'] ) ) {
				continue;
			}
			elseif ( empty( $args['type'] ) ) {
				wp_enqueue_script( $handle ); // bunlded in WP!
			}
			elseif ( $args['type'] == 'js' ) {
				wp_enqueue_script( $handle, $args['src'], $args['deps'], false, empty( $args['head'] ) );
			}
			elseif ( $args['type'] == 'css' ) {
				wp_enqueue_style( $handle, $args['src'], $args['deps'] );
			}
		}
	}


	/**
	 * Add a body class
	 *
	 * @param  string  a body class name
	 * @since  1.0.0
	 */
	public static function add_body_class( $class ) {

		if ( empty( $class ) ) {
			return;
		}

		self::$body_classes[] = $class;
	}


	/**
	 * Add body classes
	 *
	 * @param  array  $classes  existing body classes
	 * @return array            body classes + new ones
	 *
	 * @since 1.0.0
	 */
	public static function add_body_classes( $classes ) {
		return array_merge( $classes, self::$body_classes );
	}


	/**
	 * Output the layouts
	 *
	 * @since 1.0.0
	 */
	public function render() {
		echo implode( '', $this->html );
	}


	/**
	 * Render the pre-header layouts
	 *
	 * @since 1.0.0
	 */
	public function render_pre_header() {
		echo implode( '', $this->pre_html );
	}


	/**
	 * Register custom field settings
	 *
	 * Such as "flex content only" field
	 *
	 * @param array  $field  current ACF field
	 * @since 1.0.0
	 */
	public function add_field_settings( $field ) {

		// only show in flex content fields?
		acf_render_field_setting( $field, array(
			'label'			=> __( 'Only Show In Flex Content Field', '_rock' ),
			'instructions'	=> '',
			'type'			=> 'true_false',
			'name'			=> self::$setting_flex_only,
			'ui'			=> 1,
		));
	}


	/**
	 * Don't display flex-content-only field if not in flexible content
	 *
	 * @param  array  $field  current ACF field settings
	 * @return array  $field  s'all good
	 * @return bool   false   field shouldn't be shown
	 *
	 * @since  1.0.0
	 */
	public function maybe_kill_flex_field( $field ) {

		// if flex-only and has no parent layout (not in flex content)
		if ( ! empty( $field[ self::$setting_flex_only ] ) && empty( $field['parent_layout'] ) ) {
			return false;
		}

		return $field;
	}


	/**
	 * Create an instance of this class
	 *
	 * @since 1.0.0
	 */
	public static function instance() {

		if ( ! empty( self::$instance ) ) {
			return self::$instance;
		}
		return ( self::$instance = new self );
	}
}


/**
 * Return an instance of the layouts class
 *
 * @since 1.0.0
 */
function _rock_layouts() {
	return _Rock_Layouts::instance();
}
